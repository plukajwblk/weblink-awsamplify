import { Component, OnInit } from '@angular/core';
import { AmplifyService } from 'aws-amplify-angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
})
export class AuthComponent implements OnInit {

  // This is the set of fields in the SingUp panel
  private signUpConfig = {
    header: 'Welcome!',
    defaultCountryCode: '46',
    hideAllDefaults: true,
    signUpFields: [
      {
        label: 'Email',
        key: 'email',
        required: true,
        displayOrder: 1,
        type: 'string',
      },
      {
        label: 'Password',
        key: 'password',
        required: true,
        displayOrder: 2,
        type: 'password',
      },
      {
        label: 'Name',
        key: 'name',
        required: true,
        displayOrder: 3,
        type: 'string',
      },
      {
        label: 'Family name',
        key: 'family_name',
        required: true,
        displayOrder: 4,
        type: 'string',
      }
    ]
  };

  constructor(public amplifyService: AmplifyService, public router: Router) {
    this.amplifyService = amplifyService;
    this.amplifyService.authStateChange$
      .subscribe(authState => {
        if (authState.state === 'signedIn') {
          this.router.navigate(['/sample']);
        }
      });
  }

  ngOnInit() {
  }
}
