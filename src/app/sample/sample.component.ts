import { Component, OnInit } from '@angular/core';

// Imported the generated API
import { APIService } from '../API.service';

@Component({
  selector: 'app-sample',
  templateUrl: './sample.component.html',
  styleUrls: ['./sample.component.scss']
})
export class SampleComponent implements OnInit {
  // Declare variables
  allTodos: any = [];

  // The api is added in the contructor!
  constructor(private api: APIService) { }

  async ngOnInit() {
    // List all todos on the initialize of component
    const result = await this.api.ListTodos();
    this.allTodos = result.items;
  }

  // Create Todo
  async createTodo() {

    // Sample fixed todo
    const newTodo = {
      name: 'Lorem Ipsum ' + Math.floor(Math.random() * 100),
      description: 'Dolor Sit amet',
      completed: false,
    };

    // Create mutation
    const result = await this.api.CreateTodo(newTodo);

    // Push newly created todo to the todos list
    this.allTodos.push(result);
  }

  // List Todos
  async listTodos() {
    const result = await this.api.ListTodos();
    this.allTodos = result.items;
  }

}
