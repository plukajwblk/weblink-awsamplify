
# weblink-awsamplify
[TOC]
  

## 0. Overview

#### Frameworks and Libraries

##### (Frontend):

- Angular ^8.0.0
- AWS Amplify

##### (Backend)

- AWS Lambda
- Amazon API Gateway / GraphQL
- DynamoDB
- Elasticsearch (Caching)
- Amazon Cognito (Auth)

##### (Additional assumptions to improve security)

- Easy to implement HTTPS
- API Gateway endpoints protected with IAM (**3**)
- Database and Elasticsearch served over a private network and cannot be accessed over the internet.

##### Deployment Architecture:

- Website hosted in a S3 bucket
- Cloudfront in front of the S3 bucket (**1**)
- Web Application Firewall (**2**) on Cloudront
- Route 53 as DNS service

  

## 1. Required packages

### Angular CLI
```sh
npm install -g @angular/cli
```

### Amplify CLI
```sh
npm install -g @aws-amplify/cli
```

## 2. Configuring Amplify CLI
```sh
amplify configure
```
- The browser window will open and you'll be asked to login to AWS Console

- Select Region (**eu-west-1** for Ireland)

- Specify new username for IAM user (for this project). In this case I've provided **amplify-test**

- You'll be redirected to Management Console to complete the IAM user registration

- Choose **Programatic Access** and click _Next_

- In Permissions section, make sure that **AdministrationAccess** is selected

- _Next_ and _Create_

- Write down the Access key ID and Secret access key (In this case the keys are: AKIA24RSSKQ5R4TX3GEO/1SFHtXY2TfFV9YmmPHivTehasj2iN248rZQ1pc78)

- Go back to terminal/cmd and press _Enter_ to continue

- Provide the **Access Key**, **Secret Access Key ID** and **Profile Name** (something different than Default i.e. in this case I've provided _amplify-test_)

 
## 3. Project preparation

### Create a new project
```sh
ng new weblink-awsamplify --routing --style=scss --directory ./
```
### Install the required Amplify packages to Angular app
```sh
npm install --save aws-amplify aws-amplify-angular
```
### Configure the Amplify App
```sh
amplify init
```
 
### You'll be asked a series of questions:

- Project Name (_default_)
- Environment (_in this case is **dev**_)
- Default Code Editor (_Visual Studio Code_)
- Type of App (_javascript_)
- Framework (_angular_)
- Source Directory Path (_src_)
- Distribution Directory Path (_dist/weblink-awsamplify_)
- Build Command: (_default_)
- Start Command (_default_)
- After that **Use an AWS Profile** and selected the one created before.

### Create the S3 Bucket and Push the Application to the cloud
```sh
amplify hosting add
```
- We can choose DEV or PROD environment here
- Hosting bucket name (_weblink-awsamplify_) the environment will be provided by default as the suffix.
- Index doc (_index.html_)
- Error doc (_default_)

### Publish the app to the AWS
```sh
amplify publish
```
The **cloudfront endpoint** will be provided when you choose to use the PROD environment.

### Configure the Cognito service
```sh
amplify auth add
```

Sample Manual Configuration:

- Do you want to use the default authentication and security configuration? (_Manual configuration_)
- Select the authentication/authorization services that you want to use: User Sign-Up, Sign-In, connected with AWS IAM controls (_Enables per-user Storage features for images or other content, Analytics, and more_)
- Please provide a friendly name for your resource that will be used to label this category in the project: (_weblinkawsamplifyresource_)
- Please enter a name for your identity pool. (_weblink_awsamplify31543293_identitypool_31543293_)
- Allow unauthenticated logins? (_Yes_)
- Do you want to enable 3rd party authentication providers in your identity pool? (_No_)
- Please provide a name for your user pool: (_weblink_awsamplify31543293_userpool_31543293_)
- How do you want users to be able to sign in? (_Email_)
- Multifactor authentication (MFA) user login options: (_OFF_)
- Email based user registration/forgot password: Enabled (_Requires per-user email entry at registration_)
- Please specify an email verification subject: (_Your verification code_)
- Please specify an email verification message: (_Your verification code is {####}_)
- Do you want to override the default password policy for this User Pool? (_No_)
- What attributes are required for signing up? (_Email_)
- Specify the app's refresh token expiration period in days: (_30_)
- Do you want to specify the user attributes this app can read and write? (_No_)
- Do you want to enable any of the following capabilities? (_None_)
- Do you want to use an OAuth flow? (_No_)
- Do you want to configure Lambda Triggers for Cognito? (_No_)

### Publish Your changes
```sh
amplify publish
```
### Add Amplify to the Angular Application

Add the following code to **polyfills.ts** file

```ts
(window as any).global = window;

(window as any).process = {
    env: { DEBUG: undefined },
};
```

Add the following code to **main.ts** file:
```ts
import Amplify from 'aws-amplify';
import awsconfig from './aws-exports';

Amplify.configure(awsconfig);
```
Change compilerOptions in the **tsconfig.app.json** file to

```json
"compilerOptions": {
  "outDir": "./out-tsc/app",
  "types": [
    "node"
  ]
},
```
 
## 4. Implement Login and Signup pages

Generate Auth Component
```sh
ng g c auth
```
and amend **app-routing.modules.ts** file with this component
```ts
import { AuthComponent } from './auth/auth.component';

const routes: Routes = [
  {
    path: '',
    component: AuthComponent
  }
];
```
  
Add import to **app.module.ts** and fill the imports and providers in **@NGModule**
```ts
import { AmplifyAngularModule, AmplifyService } from 'aws-amplify-angular';

@NgModule({
  declarations: [
    AppComponent,
    AuthComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AmplifyAngularModule
  ],
  providers: [AmplifyService],
  bootstrap: [AppComponent]
})
```
 
The auth component code is provided below:

**auth.component.ts**:
```ts
import { Component, OnInit } from '@angular/core';
import { AmplifyService } from 'aws-amplify-angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
})
export class AuthComponent implements OnInit {
  // This sets default username to email.
  usernameAttributes = 'email';

  // This is the set of fields in the SingUp panel
  private signUpConfig = {
    header: 'Welcome!',
    defaultCountryCode: '46',
    hideAllDefaults: true,
    signUpFields: [
      {
        label: 'Email',
        key: 'username',
        required: true,
        displayOrder: 1,
        type: 'string',
      },
      {
        label: 'Password',
        key: 'password',
        required: true,
        displayOrder: 2,
        type: 'password',
      },
      {
        label: 'Name',
        key: 'name',
        required: true,
        displayOrder: 3,
        type: 'string',
      },
      {
        label: 'Family name',
        key: 'family_name',
        required: true,
        displayOrder: 4,
        type: 'string',
      },
      {
        label: 'Phone number',
        key: 'phone_number',
        required: false,
        displayOrder: 5,
        type: 'string',
      }
    ]
  };

  constructor(public amplifyService: AmplifyService, public router: Router) {
    this.amplifyService = amplifyService;
    this.amplifyService.authStateChange$
      .subscribe(authState => {
        if (authState.state === 'signedIn') {
          this.router.navigate(['/sample']);
        }
      });
}

  ngOnInit() {
  }
}
```
**auth.component.html**:
```html
<amplify-authenticator  [signUpConfig]="signUpConfig"></amplify-authenticator>
```
In order to import the styles go to **src/styles.scss** file and import:

```scss
@import "~@aws-amplify/ui/src/Theme.css";
@import "~@aws-amplify/ui/src/Angular.css";
@import "~@aws-amplify/ui/dist/style.css";
```
  
## 5. Restricting pages from the unsigned users

Create **Auth Guard Component**
```sh
ng g guard auth
```

Replace the code in generated guard (**auth.guard.ts**) to:
```ts
import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Auth } from 'aws-amplify';
@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router) {}
  canActivate(): Promise < boolean > {
    return new Promise((resolve) => {
      Auth.currentAuthenticatedUser({
          bypassCache: false
        })
        .then((user) => {
          if(user){
            resolve(true);
          }
        })
        .catch(() => {
          this.router.navigate(['/']);
          resolve(false);
        });
    });
  }
}
```

Go to **app-routing.module.ts** and import:
```ts
import { AuthGuard } from './auth.guard';
```
Then to restrict the route, simply add this line to the object:
```ts
canActivate: [AuthGuard]
```
i.e.
```json
{
  path: 'sample',
  component: SampleComponent,
  canActivate: [AuthGuard]
}
```
Done.

### Automatic Redirection After Login

_"We can accomplish this by listening to authStateChange$ events generated by Amplify library."_

Go to **auth.component.ts** file and add the following code:

```ts
import { AmplifyService } from 'aws-amplify-angular';
import { Router } from '@angular/router';


  constructor(public amplifyService: AmplifyService, public router: Router) {
    this.amplifyService = amplifyService;
    this.amplifyService.authStateChange$
      .subscribe(authState => {
        if (authState.state === 'signedIn') {
          this.router.navigate(['/sample']);
        }
      });
  }
```

## 6. Create DB and API
```sh
amplify add api
```
- Select **GraphQL** and for _Authorization Type_  choose **Amazon Cognito User Pool**
- Do you have an annotated GraphQL schema? (_N_)
- Do you want a guided schema creation? (_Y_)
- Choose type of schema that is similiar to your project
- Now you'll be asked if you want to amend default schema.
- **You can always edit this file in the future.**

After that when you type:
```sh
amplify publish
```
 
The Amplify will create the necessary DB Tables, API Endpoints etc. but firstly you'll be asked a few questions:

- Do you want to generate code for your newly created GraphQL API (_Yes_)
- Choose the code generation language target (_angular_)
- Enter the file name pattern of graphql queries, mutations and subscriptions (_src/graphql/**/*.graphql_)
- Do you want to generate/update all possible GraphQL operations - queries, mutations and subscriptions (_Yes_)
- Enter maximum statement depth [increase from default if your schema is deeply nested] (_2_)
- Enter the file name for the generated code (_src/app/API.service.ts_)

All the generated queries and mutiations will be saved in src/graphql

The DynamoDB request can be found in (_/amplify/backend/api/weblinkawsAmplify/build/resolvers_) folder

---
The file **API.service.ts** is also createad, this file contains all the queries, mutations and subscriptions ready to import to the components.

---

**WARNING:** If the API.service.ts is empty, just run this command:
```sh
amplify codegen
```

The examples of API calls can be found on sample page component.
```ts
  // Create Todo
  async createTodo() {

    // Sample fixed todo
    const newTodo = {
      name: 'Lorem Ipsum ' + Math.floor(Math.random() * 100),
      description: 'Dolor Sit amet',
      completed: false,
    };

    // Create mutation
    const result = await this.api.CreateTodo(newTodo);

    // Push newly created todo to the todos list
    this.allTodos.push(result);
  }

  // List Todos
  async listTodos() {
    const result = await this.api.ListTodos();
    this.allTodos = result.items;
  }
```

## 7. Adding Elasticsearch service Support (TODO)
### Get the Stream ARN
- Go to DynamoDB AWS page and select the table.
- Copy the **Latest stream ARN**

### Allocate the VPC address
- Search the **VPC** Service in the AWS Services
- Select **Elastic IPs** from the left menu (Virtual Private Cloud Section_)
- Click on **Allocate new address**
- Choose **Amazon pool**
- Click **Allocate** and **Save** somewhere the IP you've received.
- I highly encourage you to add a tag to this IP. i.e. **Project**.

### Create a VPC with two subnets
- Search the **VPC** Service in the AWS Services
- Click on **Launch VPC Wizard**
- Select the **VPC with Public and Private Subnets** from the tabs on the left
- Check if the proposed IP ranges doesn't not overlap with other AWS Apps
- Provide a **VPC name**
- Provide **Elastic IP Allocation ID:** and select the one created in the previous step.
- Click on **Create VPC**

### Create a security group
- Search the **VPC** Service in the AWS Services
- Select **Security Groups** from the left menu (_Security Section_)
- Click on **Create security group** button
- Provide the **Security group name** and **Description**
- Choose previously created **VPC**
- CLick on **Create**

### Create a Elasticsearch domain
- Search the **Elasticsearch** Service in the AWS Services
- Click on **Create a new domain**
- Choose environment (in this case I've choosed _Development and testing_)
- Choose the latest **Elasticsearch version**
- Click **Next**
- Provide **Elasticsearch domain name** (_weblinkamplifytest_)
- Choose **Number of instances** (_1_)
- Select the **Instance Type** (I've choosed the cheapest one: _t2.small.elasticsearch_)
- Leave rest as it is and click on **Next**
- Select **VPC access (Recommended)**
- In **VPC** dropdown select the VPC you've created in **Create a VPC with two subnets** subchapter.
- In **Subnet** select the **private** subnet from the list
- Choose the **Security Group** created in the **Create a security group** subchapter.
- On **Access policy** section click on **Select a template** dropdown and choose **Do not require signing request with IAM credential**
- Click on **Next**
- Review and **Confirm**
- **Kibana** and **Elasticsearch** will be created at this step.


## 8.  Angular Readme
### Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

### Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

### Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## 9. Online Documentation , Tutorials & Examples:

- [Amplify Documentation](https://aws-amplify.github.io/)
- [The Angular with Amplify course](https://www.youtube.com/watch?v=t4hDoMvxMWk&list=PLvmxnsCyoh64kn_H-7OJGubNMQhJzOmbh&index=1)
- [Modeling Relationships (1:1 | 1:N | M:N) in GraphQL ](https://www.youtube.com/watch?v=eUQvsuO6EnU&list=PLvmxnsCyoh64kn_H-7OJGubNMQhJzOmbh&index=10)



------------
(**1**) Cloudfront - Amazon CloudFront is a fast content delivery network (CDN) service that securely delivers data, videos, applications, and APIs to customers globally with low latency, high transfer speeds, all within a developer-friendly environment.

(**2**) WAF - AWS WAF is a web application firewall that helps protect your web applications from common web exploits that could affect application availability, compromise security, or consume excessive resources.

(**3**) IAM - AWS Identity and Access Management (IAM) enables you to manage access to AWS services and resources securely. Using IAM, you can create and manage AWS users and groups, and use permissions to allow and deny their access to AWS resources.
  


